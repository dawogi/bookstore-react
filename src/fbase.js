import Rebase from "re-base";
import firebase from "firebase";


const firebaseApp = firebase.initializeApp({
    apiKey: "AIzaSyCQCcHl93cwUn6eq4SckrXRC6f-kyckTxA",
    authDomain: "bookstore-6acb1.firebaseapp.com",
    databaseURL: "https://bookstore-6acb1.firebaseio.com",
    projectId: "bookstore-6acb1",
    storageBucket: "bookstore-6acb1.appspot.com",
    messagingSenderId: "676932864653"
});

const fbase = Rebase.createClass(firebaseApp.database());

export {fbase, firebaseApp};