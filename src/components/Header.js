import React from 'react';
import {Link} from 'react-router-dom';

class Header extends React.Component {

    constructor() {
        super();
        this.state = {
            bookstoreName: "Black Books",
            clicked: true,
            textColor: "white",
            backgroundcolor: "black"
        }
    }

    handleClick = () => {

        if (this.state.clicked) {
            this.setState({
                bookstoreName: "White Books",
                textColor: "white",
                backgroundcolor: "black"
            })
        }
        else {
            this.setState({
                bookstoreName: "Black Books",
                textColor: "black",
                backgroundcolor: "white"
            })
        }
        this.setState({
            clicked: !this.state.clicked
        })
    }

    render() {

        let headerCss = {
            color: this.state.textColor,
            backgroundColor: this.state.backgroundcolor
        }

        return (
            <div className="row header" style={headerCss} onClick={this.handleClick}>
                <center><h2>Header {this.state.bookstoreName}</h2></center>
                <Link to="/admin">Go to admin panel</Link>
            </div>
        );
    }
}

export default Header;